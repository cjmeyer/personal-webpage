# Setup ATLAS local ROOT base if necessary
[ "`compgen -a | grep localSetupROOT`x" == "x" ] \
  && echo "Going to set up ATLAS local ROOT base from cvmfs" \
  && export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase \
  && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

# Set up recent version of Git
lsetup git
