<head>
<?php
  $title = ucfirst(basename($_SERVER["SCRIPT_FILENAME"], ".php"));
  if (strcmp($title, "Index") === 0) echo "<title>Chris Meyer</title>\n";
  else echo "<title>$title | Chris Meyer</title>\n";
?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<link rel="icon" type="image/png" href="/cjmeyer/favicon.ico">


<script type="text/javascript">
    document.createElement('header'); 
    document.createElement('nav');
    document.createElement('section');
    document.createElement('footer');
</script>

<link href="https://fonts.googleapis.com/css?family=Gentium+Basic|Open+Sans" rel="stylesheet">
<link rel="StyleSheet" href="/cjmeyer/style.css" type="text/css" />

</head>
