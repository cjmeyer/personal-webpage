<!DOCTYPE html>
<html xml:lang="en" lang="en">
<?php include("head.php"); ?>

<body>

<?php include("header.php"); ?>

<?php include("nav.php"); ?>

<section id="main" role="main">
   <div id="content" class="padded">
      <h2>Selected Publications</h2>
      <p>
         ATLAS Collaboration,
         <i>Measurements of Higgs boson properties in the diphoton decay channel with 36.1 fb<sup>−1</sup> pp collision data at the center-of-mass energy of 13 TeV with the ATLAS detector</i>,
         <a href="http://cds.cern.ch/record/2273852">ATLAS-CONF-2017-045</a>.
         Submission in preparation.
      </p>
      <p>
         ATLAS Collaboration,
         <i>Search for new phenomena in high-mass diphoton final states using 37 fb<sup>−1</sup> of proton–proton collisions collected at sqrts = 13 TeV with the ATLAS detector</i>,
         <a href="https://arxiv.org/abs/1707.04147">arXiv:1707.04147 [hep-ex]</a>.
         Submitted to Phys. Lett.
      </p>
      <p>
         ATLAS Collaboration,
         <i>Search for resonances in diphoton events at sqrts = 13 TeV with the ATLAS detector</i>,
         <a href="http://dx.doi.org/10.1007/JHEP09(2016)001">JHEP <b>09</b> (2016) 01</a>,
         <a href="http://arxiv.org/abs/1606.03833">arXiv:1606.03833 [hep-ex]</a>.
      </p>
      <p>
         ATLAS Collaboration,
         <i>Measurement of the inclusive jet cross-section in proton-proton collisions at sqrts = 7 TeV using 4.5 fb<sup>-1</sup> of data with the ATLAS detector</i>,
         <a href="http://dx.doi.org/10.1007/JHEP02(2015)153">JHEP <b>02</b> (2015) 153</a> [Erratum: JHEP09,141(2015)],
         <a href="http://arxiv.org/abs/1410.8857">arXiv:1410.8857 [hep-ex]</a>.
      </p>
      <p>
         ATLAS Collaboration,
         <i>Measurement of inclusive jet and dijet production in pp collisions at sqrts = 7 TeV using the ATLAS detector</i>,
         <a href="http://dx.doi.org/10.1103/PhysRevD.86.014022">Phys. Rev. <b>D86</b> (2012) 014022</a>,
         <a href="http://arxiv.org/abs/1112.6297">arXiv:1112.6297 [hep-ex]</a>.
      </p>
      <p>
         ATLAS Collaboration,
         <i>Studies of Zγ production in association with a high-mass dijet system in pp collisions at sqrts = 8 TeV with the ATLAS detector</i>,
         <a href="http://dx.doi.org/10.1007/JHEP07(2017)107">JHEP <b>07</b> (2017) 107</a>,
         <a href="http://arxiv.org/abs/1705.01966">arXiv:1705.01966 [hep-ex]</a>.
      </p>
      <p>
         ATLAS Collaboration, <i>Jet energy measurement and its systematic uncertainty in proton-proton collisions at sqrts = 7 TeV with the ATLAS detector</i>,
         <a href="http://dx.doi.org/10.1140/epjc/s10052-014-3190-y">Eur. Phys. J. <b>C75</b> (2015) 17</a>,
         <a href="http://arxiv.org/abs/1406.0076">arXiv:1406.0076 [hep-ex]</a>.
      </p>
      <p>
         ATLAS Collaboration,
         <i>Measurement of the inclusive jet cross-sections in proton-proton collisions at sqrts = 8 TeV with the ATLAS detector</i>,
         <a href="http://dx.doi.org/10.1007/JHEP09(2017)020">JHEP <b>09</b> (2017) 020</a>,
         <a href="http://arxiv.org/abs/1706.03192">arXiv:1706.03192 [hep-ex]</a>.
      </p>
      <p>
         Author on approximately 550 ATLAS Collaboration peer-reviewed publications since October 2011.
      </p>

      <h2>Selected Proceedings</h2>
      <p>
         C. Meyer,
         <i>Measurements of photon and jet production properties with ATLAS</i>,
         <a href=http://arxiv.org/abs/1610.04220">arXiv:1610.04220 [hep-ex]</a>.
         Proceeding for LHCP 2016: 4th Conference on Large Hadron Collider Physics, Lund, Sweden, 13-18 June 2016.
      </p>
      <p>
         C. Meyer,
         <i>Recent QCD Results From ATLAS</i>,
         <a href=http://arxiv.org/abs/1409.4399">arXiv:1409.4399 [hep-ex]</a>.
         Proceeding for LHCP 2014: Second Annual Conference on Large Hadron Collider Physics, New York, United States, 2-7 June 2014.
      </p>
      <p>
         C. Meyer,
         <i>Recent QCD Results From ATLAS</i>,
         <a href=http://arxiv.org/abs/1310.2944">arXiv:1310.2944 [hep-ex]</a>.
         Proceeding for LHCP 2013: First Large Hadron Collider Physics Conference, Barcelona, Spain, 13-18 May 2013.
      </p>
      <p>
         C. Meyer,
         <i>The ATLAS Tile Calorimeter Calibration and Performance</i>,
         <a href=http://arxiv.org/abs/1310.2945">arXiv:1310.2945 [hep-ex]</a>.
         Proceeding for LHCP 2013: First Large Hadron Collider Physics Conference, Barcelona, Spain, 13-18 May 2013.
      </p>
      <p>
         C. Meyer,
         <i>Results on QCD jet production at ATLAS and CMS</i>,
         <a href=http://arxiv.org/abs/1310.2946">arXiv:1310.2946 [hep-ex]</a>.
         Proceeding for Blois 2012: 24th Rencontres de Blois on "Particle Physics and Cosmology", Blois, Loire Valley, France, 27 May - 1 Jun 2012.
      </p>
      <p>
         C. Meyer, T. Rice, L. Stevens, and B. A. Schumm,
         <i>Simulation of an All-Silicon Tracker</i>,
         <a href=http://arxiv.org/abs/0709.0758">arXiv:0709.0758 [hep-ex]</a>.
         Proceeding for the 2007 International Linear Collider Workshop, DESY, Hamburg, Germany, 30 May - 3 June 2007.
      </p>
   </div>
</section>

<?php include("footer.php"); ?>

</body>
</html>
