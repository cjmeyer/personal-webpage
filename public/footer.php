<footer id="footer">
   <div class="padded">
      &copy; 2018 Chris Meyer
      | <a href="http://www.upenn.edu">The University of Pennsylvania</a>
      | <a href="http://www.hep.upenn.edu">HEP</a>
      | <a href="mailto:chris.meyer@cern.ch">Contact</a>
   </div>
</footer>
