<!DOCTYPE html>
<html xml:lang="en" lang="en">
<?php include("head.php"); ?>

<body>

<?php include("header.php"); ?>

<?php include("nav.php"); ?>

<section id="main" role="main">
   <div id="content" class="padded">
      <h2>Selected Talks and Lectures</h2>
      <p>
         <i>Measuring the Higgs: two photon edition</i><br />
         Graduate HEP lecture, 2017, Bonn, Germany
      </p>
      <p>
         <i><a href="/cjmeyer/files/MeyerChris_20161103_BonnSeminar_HGam.pdf">Looking for bumps in diphoton events</a></i><br />
         <a href="https://www.pi.uni-bonn.de/news/events/particle-physics-seminar">Particle Physics Seminar</a>, 2016, Bonn, Germany
      </p>
      <p>
         <i><a href="http://web.fnal.gov/organization/theory/JETP/2016/MeyerChris_20160415_FermiLabSeminar_HighMass.pdf">Search for new physics in diphoton events</a></i><br />
         <a href="http://theory.fnal.gov/jetp/jetp2016/">Fermilab Joint Experimental-Theoretical Physics Seminar (Wine & Cheese Talk)</a>, 2016, Chicago, US
      </p>
      <p>
         <i><a href="http://cds.cern.ch/record/2157133">Measurements of photon and jet production properties with ATLAS</a></i><br />
         <a href="http://lhcp2016.hep.lu.se/">Large Hadron Collider Physics</a>, 2016, Lund, Sweden
      </p>
      <p>
         <i><a href="http://cds.cern.ch/record/1703729">Recent QCD results from ATLAS</a></i><br />
         <a href="http://www.bnl.gov/lhcp2014/index.php">Large Hadron Collider Physics</a>, 2014, New York, US
      </p>
      <p>
         <i><a href="https://indico.cern.ch/getFile.py/access?contribId=77&sessionId=21&resId=0&materialId=slides&confId=250727">Summary of photon, jet, and soft QCD physics results from ATLAS</a></i> (Protected) <br />
         <a href="http://ruphe.fsac.ac.ma/AtlasWeek2013/index.php">ATLAS Week</a>, 2013, Marrakech, Morocco
      </p>
      <p>
         <i><a href="http://cds.cern.ch/record/1548352">Recent QCD results from ATLAS</a></i> <br />
         <a href="http://lhcp2013.ifae.es/">Large Hadron Collider Physics</a>, 2013, Barcelona, Spain
      </p>
      <p>
         <i><a href="http://cds.cern.ch/record/1546390">The ATLAS Tile Calorimeter calibration and performance at the LHC</a></i> <br />
         <a href="http://lhcp2013.ifae.es/">Large Hadron Collider Physics</a>, 2013, Barcelona, Spain
      </p>
      <p>
         <i><a href="https://indico.cern.ch/getFile.py/access?contribId=35&resId=0&materialId=slides&confId=194205">Introduction to jet calibration and in situ calibration</a></i> (Protected) <br />
         <a href="https://indico.cern.ch/conferenceOtherViews.py?view=standard&confId=194205">Hadronic Calibration Workshop</a>, 2012, Grenoble, France (Protected)
      </p>
      <p>
         <i><a href="https://indico.bnl.gov/getFile.py/access?contribId=6&sessionId=0&resId=0&materialId=slides&confId=506">Dijet cross sections & Tile Calorimeter studies</a></i> (Protected)<br />
         <a href="https://indico.bnl.gov/conferenceDisplay.py?confId=506">Presentations to the NSF and DOE LHC Research Program Managers</a>, 2012, CERN (Protected)
      </p>
      <p>
         <i><a href="http://cds.cern.ch/record/1445574">Standard Model jet measurements using the ATLAS detector</a></i> <br />
         <a href="http://indico.cern.ch/conferenceDisplay.py?confId=179702">109th Meeting of the LHCC</a>, 2012, CERN
      </p>
      <p>
         <i><a href="http://blois.in2p3.fr/2012/transparencies/wednesday_afternoon/qcd/Meyer.pdf">Results on QCD jet production at ATLAS and CMS</a></i> <br />
         <a href="http://confs.obspm.fr/Blois2012/">24th Rencontres de Blois</a>, 2012, Blois, France
      </p>
   </div>
</section>

<?php include("footer.php"); ?>

</body>
</html>
