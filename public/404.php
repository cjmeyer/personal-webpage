<!DOCTYPE html>
<html xml:lang="en" lang="en">
<?php include("head.php"); ?>

<body>

<?php include("header.php"); ?>

<?php include("nav.php"); ?>

<section id="main" role="main">
   <div id="content" class="padded">
      <h2>Page not found</h2>
      <p>
         I'm sorry, much like a <a href="https://en.wikipedia.org/wiki/750_GeV_diphoton_excess">diphoton mass bump at 750 GeV</a> the page you're looking for doesn't seem to exist.
         Try going back, or clicking one of the links in the navigation bar above.
      </p>
      <div id="center-picture">
         <img alt="This is not the bump you're looking for" src="/cjmeyer/images/not_a_bump.png" />
      </div>
   </div>
</section>

<?php include("footer.php"); ?>

</body>
</html>
