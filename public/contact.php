<!DOCTYPE html>
<html xml:lang="en" lang="en">
<?php include("head.php"); ?>

<body>

<?php include("header.php"); ?>

<?php include("nav.php"); ?>

<section id="main" role="main">
   <div id="content" class="padded">
      <h2>Contact</h2>
      <table>
         <tr>
            <td>CERN Office:</td>
            <td><a href="https://maps.cern.ch/mapsearch/mapsearch.htm?loc=['602/R-023']">Bat. 602/R-023</a></td>
         </tr>
         <tr>
            <td>CERN Office:</td>
            <td><a href="tel:+41227674674">+41 (0) 22 767 46 74 </a></td>
         <tr>
         <tr>
            <td>CERN Mobile:</td>
            <td><a href="tel:+41754117106">+41 (0) 75 411 71 06 </a></td>
         <tr>
         </tr>
            <td>US Phone:</td>
            <td><a href="tel:+18318244848">+1 831-824-4848 </a></td>
         <tr>
         </tr>
            <td>Email:</td>
            <td><a href="mailto:chris.meyer@cern.ch">chris.meyer@cern.ch</a></td>
         </tr>
      </table>
      <br />
      <br />
   </div>
</section>

<?php include("footer.php"); ?>

</body>
</html>
