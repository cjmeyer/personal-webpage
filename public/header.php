<header id="header" role="banner">
   <div class="padded">
      <h1>Chris Meyer</h1>
      <img alt="The University of Chicago" src="/cjmeyer/images/shield-logotype-drkbkgd-RGB-4k.png" />
   </div>
</header>
