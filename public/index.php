<!DOCTYPE HTML>
<html xml:lang="en" lang="en">
<?php include("head.php"); ?>

<body>

<?php include("header.php"); ?>

<?php include("nav.php"); ?>

<section id="main" role="main">
   <div id="content" class="padded">
      <div id="right-picture">
         <img alt="Chris Meyer in ATLAS" src="/cjmeyer/images/atlas_home_crop.jpg" />
      </div>
      <h2>Welcome!</h2>
      <p>
         I'm currently a Postdoctoral Fellow at the University of Pennsylvania, researching high-energy physics at the Large Hadron Collider using the ATLAS detector.
         As a convener of the sub-group measuring the Higgs bosons decaying into two photons, I've worked on accurately measuring its properties as the amount of data collected increases.
         We also look for excesses of events at high diphoton mass to search for physics beyond the Standard Model.
         For my graduate work I performed precise measurements of jet production, allowing for tests of QCD theory and constraining parton distribution functions.
      </p>
      <p>
         In addtion, detector work is critically important to collecting the high quality data needed for physics analyses.
         Over the past few years I've coordinated colleagues from around the world in upgrading and maintaining the data acquisition system of the Transition Radiation Tracker.
         As the luminosity at the LHC increases, as well as the event trigger rate, the readout system must cope with increased data flow.
         This is mainly handled by reducing the event size and upgrading the optical readout cards to increase their transfer speed.
         Before this I was run coordinator of the Tile Calorimeter at ATLAS, working on repairing the detector during the shutdown between Run-1 and Run-2.
      </p>
      <p>
         I defended my Ph.D. thesis on the <i>Measurement of dijet cross sections in pp collisions at 7 TeV centre-of-mass energy using the ATLAS detector</i> in August 2013 at the University of Chicago.
         I received my bachelors from the University of California, Santa Cruz in Spring of 2008 with a B.S. in Physics and B.A. in Mathematics.
      </p>
      <h2>Links</h2>
      <p>
         <a href="http://atlas.cern/">ATLAS Detector</a> <br />
         <a href="http://home.cern/">European Organization for Nuclear Research (CERN)</a> <br />
         <a href="http://www.hep.upenn.edu">Penn HEP Group</a> <br />
         <a href="http://www.uchicago.edu">UChicago HEP Group</a> <br />
      </p>
   </div>
</section>

<?php include("footer.php"); ?>

</body>
</html>
