<nav id="nav" role="navigation">
   <div id="navcontainer" class="unpadded">
      <img alt="The ATLAS Detector" src="/cjmeyer/images/atlas.jpg" />
      <ul>
         <li><a href="/cjmeyer/">Home</a></li>
         <li><a href="/cjmeyer/publications.php">Publications</a></li>
         <li><a href="/cjmeyer/talks.php">Talks</a></li>
         <li><a href="/cjmeyer/files/cv.pdf">CV</a></li>
         <li><a href="/cjmeyer/contact.php">Contact</a></li>
      </ul>
   </div>
</nav>
